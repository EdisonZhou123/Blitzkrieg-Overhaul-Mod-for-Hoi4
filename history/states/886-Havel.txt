
state={
	id=886
	name="STATE_886"
	manpower = 665029
	buildings_max_level_factor = 1

	state_category = rural


	history={
		owner = GER
		buildings = {
			infrastructure = 7
			arms_factory = 1
			industrial_complex = 1
			anti_air_building = 2
			air_base = 6

		}
		add_core_of = GER

		1939.1.1 = {
			buildings = {
				air_base = 10
				anti_air_building = 5
				arms_factory = 1
				industrial_complex = 2
			}
		}
	}

	provinces={
		375 3367 11219 
	}
}
